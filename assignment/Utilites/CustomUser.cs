﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace assignment.Utilites
{
    public class CustomUser
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please Insert UserName ")]
        public string username { get; set; }
        [Required(ErrorMessage = "Please Insert fullName ")]
        public string fullname { get; set; }
        [Required(ErrorMessage = "Please Insert Password ")]
        public string password { get; set; }
        public Nullable<bool> useractive { get; set; }
        [Required(ErrorMessage = "Please Insert mobilenumber ")]
        public string mobilenumber { get; set; }
    }
}