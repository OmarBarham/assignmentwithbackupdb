﻿using assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment.Utilites
{
    public class LogError
    {
        AssignmentEntities context = new AssignmentEntities();

        public void InserErrors(Exception ex)
        {

            context.Error.Add(new Error() { ErrorName = ex.Message, Location = ex.StackTrace.ToString(), TimeError = DateTime.Now.ToString() });
            context.SaveChanges();
        }
    }
}