﻿using assignment.Models;
using assignment.Utilites;
using Assignment.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace assignment.Controllers
{
    public class serviceapiController : ApiController
    {
        UsersRepositary usersRepositary = new UsersRepositary();
        LogError Log = new LogError();
        [HttpGet]
        public IEnumerable<User> GetAllUser()
        {
            try
            {
                return usersRepositary.GetUsers();
            }
            catch (Exception ex)
            {
                Log.InserErrors(ex);
                return null;
            }
        }

        [HttpGet]
        public User GetUserById(int id)
        {
            try
            {


                return usersRepositary.GetStudentByIdd(id);
            }
            catch (Exception ex)
            {
                Log.InserErrors(ex);
                return null;
            }
        }

        [HttpPost]
        public Boolean regestration(int id, [FromBody] User user)
        {
            try
            {
                usersRepositary.Create(usersRepositary.convertusertocustom(user));
                return true;

            }
            catch (Exception ex)
            {
                Log.InserErrors(ex);
                return false;
            }
        }
    }
}
