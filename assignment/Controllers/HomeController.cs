﻿using assignment.Models;
using assignment.Utilites;
using Assignment.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment.Controllers
{
    public class HomeController : Controller
    {
        UsersRepositary usersRespository = new UsersRepositary();
        public ActionResult Index()
        {
            return View(usersRespository.GetUsers());
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login([Bind(Include = "UserName,Password")] User user)
        {

            if (ModelState.IsValid)
            {
              var item=  usersRespository.Login(user);
                if (item != null)
                {
                    if (item.useractive == true)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "the user not active ");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "username or password not correct ");
                }
           

            }
            return View();
        }

        [ActionName("Edit")]
        [HttpGet]
        public ActionResult EditDetails(int Id)
        {
            if (Id != 0)
            {
                var item = usersRespository.GetStudentByIdd(Id);
                return View(usersRespository.convertusertocustom(item));
            }
            return View("Index");
        }
        [HttpPost]
         public ActionResult Edit([Bind(Include = "ID,UserName,Password,fullname,password,useractive,mobilenumber")] CustomUser user)
        {
            if (ModelState.IsValid)
            {
                usersRespository.convertcustomusertouser(user);
                usersRespository.Update(usersRespository.convertcustomusertouser(user));
                usersRespository.Save();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
     
        [HttpGet]
        public ActionResult Create()
        {
           return View();
        }
        [HttpPost]
        public ActionResult Create([Bind(Include = "UserName,Password,fullname,password,useractive,mobilenumber")] CustomUser user)
        {
            if (ModelState.IsValid)
            {
                //usersRespository.convertcustomusertouser(user);
                usersRespository.Create(user);
                usersRespository.Save();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Details(int Id)
        {
            return View(usersRespository.GetStudentByIdd(Id));
        }
       
    }
}