﻿using assignment.Models;
using assignment.Utilites;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Assignment.Repository
{
    public class UsersRepositary : IUser
    {
        AssignmentEntities context = null;
        LogError Log = null;
        public UsersRepositary()
        {
            context = new AssignmentEntities();
            Log = new LogError();
        }

        public void Create(CustomUser user)
        {
            try
            {
                context.User.Add(new User() { fullname = user.fullname, mobilenumber = user.mobilenumber, password = user.password, useractive = user.useractive, username = user.username });

            }
            catch (Exception ex)
            {
                Log.InserErrors(ex);
            }
        }

        public void Delete(int Id)
        {
            try
            {
                User student = context.User.Find(Id);
                context.User.Remove(student);
            }
            catch (Exception ex)
            {
                Log.InserErrors(ex);
            }
        }

        public User GetStudentByIdd(int Id)
        {
            try
            {
                return context.User.Find(Id);
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);
                return null;
            }
        }

        public IEnumerable<User> GetUsers()
        {
            try
            {
                return context.User.ToList();
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);
                return null;
            }
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);

            }
        }

        public void Update(User user)
        {
            try
            {
                context.Entry(user).State = EntityState.Modified;
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);

            }
        }
        public User Login(User user)
        {
            try
            {

                User item = context.User.FirstOrDefault(x => x.username == user.username && x.password == user.password);
                return item;
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);
                return null;
            }
        }
        public CustomUser convertusertocustom(User user)
        {
            try
            {
                return new CustomUser() { ID = user.ID, fullname = user.fullname, mobilenumber = user.mobilenumber, password = user.password, useractive = user.useractive, username = user.username };
            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);
                return null;
            }
        }
        public User convertcustomusertouser(CustomUser user)
        {
            try
            {
                return new User() { ID = user.ID, fullname = user.fullname, mobilenumber = user.mobilenumber, password = user.password, useractive = user.useractive, username = user.username };

            }
            catch (Exception ex)
            {

                Log.InserErrors(ex);
                return null;
            }
        }
    }
}