﻿using assignment.Models;
using assignment.Utilites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Repository
{
    interface IUser
    {
        void Create(CustomUser user);
        User GetStudentByIdd(int Id);
        void Update(User user);
        void Delete(int Id);
        IEnumerable<User> GetUsers();
        void Save();
    }
}
